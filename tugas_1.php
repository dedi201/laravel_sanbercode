<?php
abstract class Hewan
{
    public $nama,
           $darah = 50,
           $jumlahKali,
           $keahlian;

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    public function atraksi(){
        $str = $this->nama . " sedang " .$this->keahlian;
        return $str;
    }
}

trait Fight 
{
    public $attackPower,
           $defencePower;

    public function serang($hewan)
    {
        echo "$this->nama sedang menyerang $hewan->nama" . "\n";
        echo "=========================================". "\n";
    }

    public function diserang()
    {

    }

}

class Elang extends Hewan {

    use Fight;
    function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKali = 2;
        $this->keahlian = "Terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
}

class Harimau extends Hewan {

    use Fight;
    function __construct($nama)
    {
        parent::__construct($nama);
        $this->jumlahKali = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
}

$elang = new Elang(2,"terbang Tinggi",10,5);


$harimau = new Harimau(4,"lari cepat",7,8);